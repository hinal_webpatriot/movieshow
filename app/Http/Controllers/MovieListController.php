<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
class MovieListController extends Controller
{

    public function __construct()
    {
    	$this->middleware('guest');
    }

    public function index()
    {
    	// call API to get show list
    	$showDetails = $this->callShowListAPI();
    	$data['genres'] = array();
    	if(count($showDetails)>0)
		{
			$i = 0;
			foreach ($showDetails as $key => $detail) {
				// fetched only genre
				$data['genres']  =array_merge($data['genres'],$detail->genres);

			}
		}
		// get unique genre
		$data['genres'] = array_unique($data['genres']);
		return view('movie.list',$data);
	}

	public function getShowListDetails(Request $request)
	{
		// get param details
		$genre = strtolower($request->input('genre'));
		$showTime = $request->input('showTime');
		
		// manually validating inputs
		$errorMessage = array();
		if(trim($genre) == "")
		{
			$errorMessage[] = "Please enter genre";
		}
		if(trim($showTime) == "")
		{
			$errorMessage[] = "Please enter show time";
		}
		if(count($errorMessage) > 0)
		{
			return json_encode($errorMessage);
		}

		$showTime = date('H:i',strtotime($request->input('showTime')));
		// get time of after 30 minute
		$showTime = strtotime('+30 minutes',strtotime($showTime)); 

		// call API function to get data
		$listDetails = $this->callShowListAPI();
		$data = array();

		// check success status
		if(count($listDetails)>0)
		{
			// sorting filter list by rating in desc order
			$rating = array_column($listDetails, 'rating');
			array_multisort($rating, SORT_DESC, $listDetails);
			$i = 0;
			foreach ($listDetails as $key => $detail) {
				$showGenres = array_map('strtolower', $detail->genres);
				$showTimings = array_map(function($time) {
				    $startTime = explode('+', $time);
				    return strtotime($startTime[0]);
				}, $detail->showings);
				sort($showTimings); // sorting time in ascending order

				// compare genre 
				if(in_array($genre, $showGenres))
				{
					foreach ($showTimings as $key => $time) {
						// compare show start time 
						if ($showTime <= $time) {
							$data[] = $detail->name.", showing at ".date('g:ia',$time);
							$i++;
							break;
						}
					}
				}
			}
		}
		return json_encode($data);
	}

	public function callShowListAPI()
	{
		$url = config('app.showListAPI'); // get API URL
		$ch = curl_init(); // Intialized curl

		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // To disable SSL Cert checks
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $url);

		$data = array();
		try{
			$response = curl_exec($ch);
			$error = curl_error($ch);
			$resultStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			if ($resultStatus == 200) {
		    	$data = json_decode($response);
		    } else {
		        throw new Exception("Some error occurred while getting movies list");
		    }				
		}
		catch(Exception $e){
			curl_close($ch);
			die($e->getMessage());
		}
		curl_close($ch);

		return $data;
	}
}
?>