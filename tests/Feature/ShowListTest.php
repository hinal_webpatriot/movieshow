<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ShowListTest extends TestCase
{
    // test input value
    protected $inputArray = [
                                [
                                    'genre' => '',
                                    'showTime' => '',
                                    'output' =>  [ "Please enter genre","Please enter show time"]
                                ],
                                [
                                    'genre' => 'Comedy',
                                    'showTime' => '12:00',
                                    'output' => ["Zootopia, showing at 7:00pm","Shaun The Sheep, showing at 7:00pm"]
                                ],
                                [
                                    'genre' => 'Drama',
                                    'showTime' => '20:00',
                                    'output' =>  ["Moonlight, showing at 8:30pm"]
                                ],
                                [
                                    'genre' => 'Drama',
                                    'showTime' => '21:00',
                                    'output' =>  []
                                ]
                            ];

    /** @test */
    public function listPageRouteTest()
    {
        // Test list page route
        $response = $this->get('/');

        $response->assertStatus(200)
                 ->assertSee('Drama')
                 ->assertSee('Action &amp; Adventure')
                 ->assertSee('Animation')
                 ->assertSee('Comedy')
                 ->assertSee('Science Fiction &amp; Fantasy');
    }

    /** @test */
    public function validateInputTest()
    {
        // Test movie reccommendation based on genre and timings
        // get sample inputs from protected array and run mutiple test cases for each
        foreach ($this->inputArray as $key => $value) {
            $response = $this->json('GET', '/getShowList', $value);
            $this->assertEquals(json_encode($value['output']),($response->original));
            $response->assertStatus(200);
        }
    }
}
