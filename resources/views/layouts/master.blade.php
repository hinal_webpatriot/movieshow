<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Movie Recommendations</title>

        <link rel="stylesheet" href="{{ url('/css/custom.css') }}">
        <link rel="stylesheet" href="{{ url('/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ url('/css/bootstrap-clockpicker.min.css') }}">
  
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }
        </style>
    </head>
    <body>
    	@yield('content')
    	<script type="text/javascript">
  			 var baseURL = "{{ url('/') }}/";
  		</script>
    	<script src="{{ url('/js/jquery.min.js') }}"></script>
  		<script src="{{ url('/js/bootstrap.min.js') }}"></script>
  		<script src="{{ url('/js/bootstrap-clockpicker.min.js') }}"></script>
  		<script src="{{ url('/js/custom.js') }}"></script>
  		
	</body>
</html>