@extends('layouts.master')
@section('content')
    <div class="container">
        <h2 align="center">Movie Recommendations</h2>
        <hr>
        @if(count($genres) > 0)
        <div class="row">
          <div class="col-sm-12 form-div">
            <div class="row">
                @foreach($genres as $key => $genre)
                  <div class="col-md-3">
                    <a class="genreLink">
                        <!-- Generate random color --> 
                        <div class="well" style="background-color: {{'#' . str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT)}}">
                          <h4 class="text-danger">{{ $genre }}</h4>
                          <input type="hidden" class="selectedGenre" value="{{ $genre }}">
                        </div>
                    </a>
                  </div>
                @endforeach
            </div><!--/row-->    
          </div><!--/col-12-->
        </div><!--/row-->
        @endif
        <div class="col-md-12 form-div">

            <div class="col-md-6 form-element">
                <div class="form-group hide">
                  <label for="pwd">Show Time:</label>
                  <input id="showTime" readonly="" name="showTime" placeholder="Select Time"  type="text" class="form-control clockpicker " required="" />
                  <span class="error" id="showTimeError"></span>
                </div>
            </div>

        </div>
        <div class="col-md-12 form-div hide list-table-section">
            <div class="col-md-6 form-element table-responsive">
                <table class="table">
                    <thead>
                      <tr>
                        <th>Sr No.</th>
                        <th>Show Name</th>
                      </tr>
                    </thead>
                    <tbody id="showListData">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
  </div>
  @endsection
