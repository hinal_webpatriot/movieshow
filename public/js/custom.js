
// clock time picker
$(".clockpicker").clockpicker({
    placement: "bottom",
    align: "left",
    autoclose: !0,
    default: "now",
    afterDone: function() {
       getShowDetails();    
    }
});

// highlight genre on select 
$(document).on("click",".genreLink",function(){
    // close challan model of invoice module
    $(".genreLink").removeClass("active");
    $(this).addClass("active");
    $("#showTime").parents(".form-group").removeClass("hide"); //show time picker
    if($("#showTime").val() == ""){
	    $("#showTime").trigger("click");
    }
    else
    {
    	// validate input and 
    	getShowDetails();
    }
});

// get show list using ajax
function getShowDetails() {
	// get input details
	var showTime = $("#showTime").val();
	var genre = $(".genreLink.active").find('.selectedGenre').val();

	// Call api to get data
	 $.ajax({
        type: "GET",
        url: baseURL+'getShowList',
        dataType: 'json',
        data:{
        	genre : genre,
        	showTime: showTime
        },
        beforeSend: function() {
            $(".loading-process").show();
        },
        complete: function() {
            $(".loading-process").hide();
        },
        success: function(result) {
        	var tableRow = "";
        	$(".list-table-section").removeClass("hide");
        	// No Movie Recommendations
        	if(result.length > 0){
	        	$.each(result, function(key, list) {
	        		tableRow += "<tr><td>"+(key+1)+"</td><td>"+list+"</td></tr>";
	            });
	            $("#showListData").html(tableRow);
	        }
	        else
	        {
	        	$("#showListData").html("<tr colspan='2' align='center'>No Movie Recommendations</tr>");
	        }
            
        },
        error: function(jqXHR, exception) 
        {
            alert(jqXHR.responseJSON.message);
        }
    });                           
}