<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

	// Route::get('{locale}/contract', 'Auth\ContractController@redirectLink');
	Route::get('/', 'MovieListController@index');
	Route::get('/getShowList', 'MovieListController@getShowListDetails');
	// Route::get('specials/{id}', 'Post\DetailsController@showImages1');
	// Route::get('string-translations/{locale}/{filename}/{ext1?}/{ext2?}/{ext3?}', 'StringTranslationController@readFromLanguageFile');

	// Route::get('images/{plugin}/{filename}', 'PageController@setupImageRoutes');
	// Route::get('assets/{plugin}/{type}/{file}', 'PageController@setupAssetsRoutes');

